package com.shigengyu.patternpractise.models;

import com.shigengyu.patternpractise.TestConstants;
import com.shigengyu.patternpractise.repositories.TableMetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
@ContextConfiguration(locations = TestConstants.SPRING_CONFIG_FILE_NAME)
public class TableMetaRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TableMetaRepository tableMetaRepository;

    @Test
    public void getTableMetaList_AllRetrieved(){
        Assert.assertEquals(2, tableMetaRepository.getTableMetaList().size());
    }

    @Test
    public void getDataSource_ObjectInjected(){
        Assert.assertNotNull(tableMetaRepository.getDataSource());
    }
}
