package com.shigengyu.patternpractise.models.dag.builders;

import com.shigengyu.patternpractise.models.dag.DoubleInputTrainingDag;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

@Test
public class DagBuilderFactoryTest {

    @Test
    public void build() {
        DagBuilder<DoubleInputTrainingDag> dagBuilder = new DagBuilderFactory(DoubleInputTrainingDag.class).build();
        Assert.notNull(dagBuilder);

        DoubleInputTrainingDag dag = dagBuilder.build();
        Assert.isInstanceOf(DoubleInputTrainingDag.class, dag);
    }
}
