package com.shigengyu.patternpractise.config;

import com.shigengyu.patternpractise.config.PropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.shigengyu.patternpractise.TestConstants;

@Test
@ContextConfiguration(locations = TestConstants.SPRING_CONFIG_FILE_NAME)
public class PropertiesRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private PropertiesRepository propertiesRepository;

    @Test
    public void getProperty_GotPropertyAsExpected() {
        Assert.assertEquals(propertiesRepository.getProperty("name"), "Gengyu Shi");
    }
}
