package com.shigengyu.patternpractise;

public abstract class TestConstants {

    public static final String SPRING_CONFIG_FILE_NAME = "classpath:spring-test-config.xml";
}
