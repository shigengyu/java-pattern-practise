package com.shigengyu.patternpractise.common;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class ResourceTextFileTest {

    @Test
    public static void load_TextLoaded() {
        Assert.assertEquals("Line 1" + System.getProperty("line.separator") + "Line 2", ResourceTextFile.load("dag/Sample.dag"));
    }
}
