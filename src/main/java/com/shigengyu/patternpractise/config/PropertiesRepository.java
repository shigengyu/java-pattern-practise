package com.shigengyu.patternpractise.config;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.PostConstruct;

import com.shigengyu.patternpractise.common.PractiseLogger;
import com.shigengyu.patternpractise.common.WrappedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Repository;


@Repository
public class PropertiesRepository {

    private static final Logger LOGGER = PractiseLogger.getLogger();

    private Properties mergedProperties;

    public String getProperty(String key) {
        return mergedProperties.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        return mergedProperties.getProperty(key, defaultValue);
    }

    @PostConstruct
    private PropertiesRepository initialize() {
        try {
            ClassPathResource resource = new ClassPathResource("config-default.properties");
            Properties properties = PropertiesLoaderUtils.loadProperties(resource);
            LOGGER.info("Properties loaded from [{}]", resource.getFilename());

            // Try get config mode from VM arguments first
            String configMode = System.getProperty("config.mode");

            // If config mode not specified in VM arguments, find from environment variables
            if (StringUtils.isBlank(configMode)){
                configMode = System.getenv("config.mode");
            }

            if (!StringUtils.isBlank(configMode)) {
                ClassPathResource configModeResource = new ClassPathResource("config-" + configMode + ".properties");
                Properties configModeProperties = PropertiesLoaderUtils.loadProperties(configModeResource);
                LOGGER.info("Properties loaded from [{}]", configModeResource.getFilename());

                for (Entry<Object, Object> entry : configModeProperties.entrySet()) {
                    properties.put(entry.getKey(), entry.getValue());
                }
            }

            mergedProperties = properties;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw WrappedException.insteadOf(e);
        }

        return this;
    }
}
