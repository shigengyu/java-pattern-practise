package com.shigengyu.patternpractise.models.dag.builders;

import com.shigengyu.patternpractise.common.WrappedException;
import com.shigengyu.patternpractise.models.dag.Dag;

public class DagBuilder<T extends Dag> {

    private T dag;

    public DagBuilder(Class<T> clazz) {
        try {
            dag = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw WrappedException.insteadOf(e);
        }
    }

    public DagBuilder<T> withData() {
        return this;
    }

    protected void check() {
    }

    public T build() {
        check();

        return dag;
    }
}
