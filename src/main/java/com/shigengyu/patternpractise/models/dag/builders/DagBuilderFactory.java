package com.shigengyu.patternpractise.models.dag.builders;

import com.shigengyu.patternpractise.models.dag.Dag;

public class DagBuilderFactory<T extends Dag> {

    private DagBuilder<T> object;

    public DagBuilderFactory(Class<T> clazz) {
        object = new DagBuilder<>(clazz);
    }

    public DagBuilder<T> build() {
        return object;
    }
}
