package com.shigengyu.patternpractise.models.nodes;

public abstract class SingletonFactoryBase<T> {

    private T instance;

    protected abstract T createInstance();

    public synchronized final T getInstance(){
        if (instance == null) {
            instance = createInstance();
        }

        return instance;
    }
}
