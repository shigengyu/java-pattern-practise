package com.shigengyu.patternpractise.models.nodes.properties;

import com.shigengyu.patternpractise.models.nodes.DagNode;

public class FEDagPropertyDefaultValues extends DagNodePropertyDefaultValues {

    public FEDagPropertyDefaultValues(DagNode dagNode) {
        super(dagNode);
    }

    @Override
    protected void populateDefaultValues(DagNodeAdvancedProperties advancedProperties) {
        advancedProperties.set(DagNodeAdvancedPropertyTabs.TAB1, DagNodeProperties.FOO, "Wenqian")
                .set(DagNodeAdvancedPropertyTabs.TAB2, DagNodeProperties.BAR, "Beijing");
    }
}
