package com.shigengyu.patternpractise.models.nodes;

import com.shigengyu.patternpractise.models.nodes.properties.DagNodeAdvancedProperties;

public abstract class DagNodeBase implements DagNode {

    private DagNodeAdvancedProperties advancedProperties = new DagNodeAdvancedProperties();

    public DagNodeAdvancedProperties getAdvancedProperties() {
        return advancedProperties;
    }
}
