package com.shigengyu.patternpractise.models.nodes;

import com.shigengyu.patternpractise.models.nodes.properties.LRDagPropertyDefaultValues;

public class LRDagNodeFactory extends SingletonFactoryBase<LRDagNode> implements DagNodeFactory {

    @Override
    public DagNode createNode() {
        DagNode node = getInstance();
        new LRDagPropertyDefaultValues(node).populate();
        return node;
    }

    @Override
    protected LRDagNode createInstance() {
        return new LRDagNode();
    }
}
