package com.shigengyu.patternpractise.models.nodes;

import org.springframework.stereotype.Repository;
import org.testng.collections.Maps;

import java.util.Map;

@Repository
public class DagNodeRepository {

    private Map<DagNodes, DagNode> dagNodes = Maps.newHashMap();

    public synchronized DagNode getNode(DagNodes node) {
        if (!dagNodes.containsKey(node)) {
            dagNodes.put(node, node.getFactory().createNode());
        }

        return dagNodes.get(node);
    }
}
