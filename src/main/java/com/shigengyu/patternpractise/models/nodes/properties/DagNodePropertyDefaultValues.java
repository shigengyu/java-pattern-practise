package com.shigengyu.patternpractise.models.nodes.properties;

import com.shigengyu.patternpractise.models.nodes.DagNode;

public abstract class DagNodePropertyDefaultValues {

    private DagNodeAdvancedProperties advancedProperties;

    protected DagNodePropertyDefaultValues(DagNode dagNode) {
        this.advancedProperties = dagNode.getAdvancedProperties();
    }

    protected abstract void populateDefaultValues(DagNodeAdvancedProperties advancedProperties);

    public final DagNodeAdvancedProperties populate() {
        populateDefaultValues(this.advancedProperties);
        return advancedProperties;
    }
}
