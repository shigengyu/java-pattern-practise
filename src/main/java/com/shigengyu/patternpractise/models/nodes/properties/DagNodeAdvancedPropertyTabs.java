package com.shigengyu.patternpractise.models.nodes.properties;

import org.openqa.selenium.By;

public enum DagNodeAdvancedPropertyTabs {

    TAB1(By.id("tab1-id")),
    TAB2(By.id("tab2-id")),
    TAB3(By.id("tab3-id"));

    private By seleniumSelector;

    public By getSeleniumSelector() {
        return seleniumSelector;
    }

    DagNodeAdvancedPropertyTabs(By seleniumSelector) {
        this.seleniumSelector = seleniumSelector;
    }
}
