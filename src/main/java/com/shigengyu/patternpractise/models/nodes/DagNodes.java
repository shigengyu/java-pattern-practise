package com.shigengyu.patternpractise.models.nodes;

import com.shigengyu.patternpractise.common.WrappedException;

public enum DagNodes {

    FE(FEDagNodeFactory.class),

    LR(LRDagNodeFactory.class);

    private DagNodeFactory factory;

    DagNodes(Class<? extends DagNodeFactory> factoryClass) {
        try {
            factory = factoryClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw WrappedException.insteadOf(e);
        }
    }

    public DagNodeFactory getFactory() {
        return factory;
    }
}
