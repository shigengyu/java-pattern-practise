package com.shigengyu.patternpractise.models.nodes;

import com.shigengyu.patternpractise.models.nodes.properties.DagNodeAdvancedProperties;

public interface DagNode {

    String getName();

    DagNodeAdvancedProperties getAdvancedProperties();
}
