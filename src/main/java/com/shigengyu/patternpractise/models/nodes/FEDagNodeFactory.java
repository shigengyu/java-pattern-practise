package com.shigengyu.patternpractise.models.nodes;

import com.shigengyu.patternpractise.models.nodes.properties.FEDagPropertyDefaultValues;

public class FEDagNodeFactory extends SingletonFactoryBase<FEDagNode> implements DagNodeFactory {

    @Override
    public DagNode createNode() {
        DagNode node = getInstance();
        new FEDagPropertyDefaultValues(node).populate();
        return node;
    }

    @Override
    protected FEDagNode createInstance() {
        return new FEDagNode();
    }
}
