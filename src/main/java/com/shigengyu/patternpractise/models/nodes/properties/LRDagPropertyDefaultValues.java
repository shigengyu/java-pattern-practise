package com.shigengyu.patternpractise.models.nodes.properties;

import com.shigengyu.patternpractise.models.nodes.DagNode;

public class LRDagPropertyDefaultValues extends DagNodePropertyDefaultValues {

    public LRDagPropertyDefaultValues(DagNode dagNode) {
        super(dagNode);
    }

    @Override
    protected void populateDefaultValues(DagNodeAdvancedProperties advancedProperties) {
    }
}
