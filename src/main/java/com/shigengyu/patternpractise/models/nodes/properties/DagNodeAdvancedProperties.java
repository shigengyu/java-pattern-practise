package com.shigengyu.patternpractise.models.nodes.properties;


import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class DagNodeAdvancedProperties {

    private SortedMap<DagNodeAdvancedPropertyTabs, Map<DagNodeProperties, String>> map = new TreeMap<>();

    public <T> DagNodeAdvancedProperties set(DagNodeAdvancedPropertyTabs tab, DagNodeProperties property, T value) {
        if (!map.containsKey(tab)) {
            map.put(tab, new HashMap<>());
        }
        map.get(tab).put(property, value.toString());
        return this;
    }

    public SortedMap<DagNodeAdvancedPropertyTabs, Map<DagNodeProperties, String>> getMap() {
        return map;
    }
}
