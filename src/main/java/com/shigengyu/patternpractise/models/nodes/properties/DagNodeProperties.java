package com.shigengyu.patternpractise.models.nodes.properties;

import org.openqa.selenium.By;

public enum DagNodeProperties {

    FOO(By.xpath("//div/input")),
    BAR(By.xpath("//input[@class='any-input]"), 2),
    BY_ID_PROPERTY(By.id("id"));

    private By seleniumSelector;
    private Integer index;

    public By getSeleniumSelector() {
        return seleniumSelector;
    }

    public Integer getIndex() {
        return index;
    }

    DagNodeProperties(By seleniumSelector) {
        this.seleniumSelector = seleniumSelector;
    }

    DagNodeProperties(By seleniumSelector, Integer index) {
        this(seleniumSelector);
        this.index = index;
    }
}
