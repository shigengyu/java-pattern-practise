package com.shigengyu.patternpractise.models.nodes.properties;

import com.codeborne.selenide.SelenideElement;
import com.shigengyu.patternpractise.models.nodes.DagNode;

import java.util.Map;
import java.util.SortedMap;

import static com.codeborne.selenide.Selenide.$;

public class DagNodePropertiesSetter {

    private DagNode dagNode;

    public DagNodePropertiesSetter(DagNode dagNode) {
        this.dagNode = dagNode;
    }

    public DagNodePropertiesSetter apply() {
        SortedMap<DagNodeAdvancedPropertyTabs, Map<DagNodeProperties, String>> tabsMap = dagNode.getAdvancedProperties().getMap();
        for (DagNodeAdvancedPropertyTabs tab : tabsMap.keySet()) {
            // Click on the tab
            $(tab.getSeleniumSelector()).click();

            // Set properties for the tab
            Map<DagNodeProperties, String> propertiesMap = tabsMap.get(tab);
            for (DagNodeProperties property : propertiesMap.keySet()) {
                SelenideElement element;

                if (property.getIndex() != null) {
                    // Set property with index
                    element = $(propertiesMap.get(property), property.getIndex());
                } else {
                    // Set property without index
                    element = $(propertiesMap.get(property));
                }

                // TODO: Switch to the ugly method...
                element.setValue(propertiesMap.get(property));
            }
        }

        return this;
    }
}
