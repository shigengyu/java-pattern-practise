package com.shigengyu.patternpractise.models.nodes;

public interface DagNodeFactory {

    DagNode createNode();
}
