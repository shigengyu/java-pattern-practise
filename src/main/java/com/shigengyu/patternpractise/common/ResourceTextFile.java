package com.shigengyu.patternpractise.common;

import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Map;

public abstract class ResourceTextFile {

    private static final Map<String, String> map = Maps.newHashMap();

    public static final String load(String path) {
        if (!map.containsKey(path)) {
            ClassPathResource resource = new ClassPathResource(path);
            final String content;
            try {
                content = IOUtils.toString(resource.getInputStream(), "UTF-8");
            } catch (IOException e) {
                throw WrappedException.insteadOf(e);
            }

            map.put(path, content);
        }

        return map.get(path);
    }
}
