package com.shigengyu.patternpractise.repositories;

import com.shigengyu.patternpractise.models.TableMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TableMetaRepository {

    @Autowired
    private List<TableMeta> tableMetaList;

    @Autowired
    @Qualifier("dataSource1")
    private TableMeta dataSource;

    public List<TableMeta> getTableMetaList() {
        return tableMetaList;
    }

    public TableMeta getDataSource() {
        return dataSource;
    }
}
