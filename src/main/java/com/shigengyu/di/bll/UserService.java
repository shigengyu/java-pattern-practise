package com.shigengyu.di.bll;

import com.shigengyu.di.dto.User;

import java.util.List;

public interface UserService {

    List<User> getUsers();
}
