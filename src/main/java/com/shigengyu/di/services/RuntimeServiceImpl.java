package com.shigengyu.di.services;

import com.shigengyu.di.bll.UserService;
import com.shigengyu.di.dto.User;

import java.util.List;

public class RuntimeServiceImpl implements RuntimeService {

    private UserService userService;

//    public RuntimeServiceImpl(UserService userService) {
//        this.userService = userService;
//    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<User> getUsers() {
        return this.userService.getUsers();
    }
}
