package com.shigengyu.di.services;

import com.shigengyu.di.dto.User;

import java.util.List;

public interface RuntimeService {

    List<User> getUsers();
}
