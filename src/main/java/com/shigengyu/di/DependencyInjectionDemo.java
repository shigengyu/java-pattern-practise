package com.shigengyu.di;

import com.shigengyu.di.bll.UserService;
import com.shigengyu.di.dto.User;
import com.shigengyu.di.services.RuntimeService;
import com.shigengyu.di.services.RuntimeServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.InvocationTargetException;

public class DependencyInjectionDemo {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

//        runWithoutSpring();

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring/spring-main.xml");
        RuntimeService runtimeService = applicationContext.getBean(RuntimeServiceImpl.class);
        System.out.println(runtimeService.getUsers());
    }

    public static void runWithoutSpring() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        UserService userService = (UserService) Class.forName("com.shigengyu.di.bll.UserServiceImpl").getConstructor().newInstance();

        RuntimeService runtimeService = (RuntimeService) Class.forName("com.shigengyu.di.services.RuntimeServiceImpl").getConstructor(UserService.class).newInstance(userService);

//        RuntimeServiceImpl runtimeService = (RuntimeServiceImpl) Class.forName("com.shigengyu.di.services.RuntimeServiceImpl").getConstructor().newInstance();

        System.out.print(runtimeService.getUsers());
    }
}
