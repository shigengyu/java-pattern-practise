package com.wenqian.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("b");
        list.add("c");
        list.add("a");
        list.add("a");
        list.add("a");

        Map<String, Integer> results = new HashMap<String, Integer>();
        for (String str : list) {
            Integer count = results.get(str);
            results.put(str, (count == null) ? 1 : count + 1);
        }

        for (String s : results.keySet()) {
            System.out.println("The count of " + s + " is " + results.get(s));
        }
    }
}
