package com.wenqian.practice;

// 某元素左边之和等于右边之和。例如：[1, 3, 6, 2, 2], 找出元素6。
public class CentralValue {
    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 6, 2, 2};
        Integer number = getCentralValue(array);
        System.out.println(number);
    }

    public static Integer func(int[] array) {
        if (array.length == 0) {
            throw new NullPointerException("The input list is null.");
        }


        int index = 0;
        for (int i = 1; i < array.length; i++) {
            int totalleft = 0;
            int totalright = 0;
            for (int left = 0; left < i; left++) {
                totalleft += array[left];
            }
            for (int right = i + 1; right < array.length; right++) {
                totalright += array[right];
            }
            if (totalleft == totalright) {
                index = i;
            }
        }
        return array[index];
    }

    public static Integer getCentralValue(int[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Input array cannot be null or empty");
        }

        if (array.length == 1) {
            return array[0];
        }

        Integer result = null;
        int left = 0;
        int right = array.length - 1;
        int leftSum = 0;
        int rightSum = 0;

        while (left < right) {
            if (leftSum < rightSum) {
                leftSum += array[left++];
            } else {
                rightSum += array[right--];
            }

            if (leftSum == rightSum && left == right) {
                result = array[left];
                break;
            }
        }

        return result;
    }
}
